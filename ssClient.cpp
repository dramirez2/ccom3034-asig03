// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es el archivo cliente del SuperString con la
// implementacion del evalInfix. Utiliza assert para verificar que este bien
// 
// ====================================================================
#include <assert.h>
#include <iostream>
#include "superSt.h"

using namespace std;
void testEvalInfix() {
    int res;
	//Tests the assert for first infix expression
    SuperString st =(string) "( 9 + 5 ) / ( 10 - 3 )";
    cout << "Testing assertion for " << st << "... " << endl;
    assert(st.evalInfix(res));

    assert(res == 2);
	
    //Tests the assert for the seconf infix expression
    st =(string) "( 9 + 5 * 1 ) * ( 1 - 2 )";
    cout << "Testing assertion for " << st << "... " << endl;
    assert(st.evalInfix(res));
    assert(res == -14);

    st = (string)"( 9 + 5 * 1 ))";
	cout << "Testing assertion for " << st << "... " << endl;
    // this evalInfix should be false because of 
    // parenthesis unbalance
    assert(st.evalInfix(res)==false);  
	cout << "Test passed!" << endl;

}

int main(){
	
	//Calls the function that tests if evalInfix works
	testEvalInfix();
	return 0;
}

