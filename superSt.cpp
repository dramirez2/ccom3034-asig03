// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es un archivo que contiene la implementacion del
// evalinfix.
//      
// ===========================================================================
#include "superSt.h"
#include <vector>
#include <cstdlib>
#include <sstream>
#include <stack>


// Tokenizes (splits) the SuperString, based on the delimiter
// Parameters: delim: the delimiter
// Returns: a vector of SuperStrings that were split

vector<SuperString> SuperString::tokenize(const string & delim) {
	vector<SuperString> tokens;
	size_t p0 = 0, p1 = string::npos;
	while(p0 != string::npos) {
    	p1 = find_first_of(delim, p0);
    	if(p1 != p0) {
      		string token = substr(p0, p1 - p0);
      		tokens.push_back(token);
      	}
		p0 = find_first_not_of(delim, p1);
	}
	return tokens;
}

// Overload of the operator= for using stataments such as:
// SuperString st = static_cast<string>("3 4 5 + * 1 +");
SuperString& SuperString::operator= (string s) {
	this->assign(s);
	return *this;
}


// Overload of the operator= for using stataments such as:
// SuperString st = 209;
SuperString& SuperString::operator= (int n) {
	this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream() 
   		<< std::dec << n ) ).str());
	return *this;
}

SuperString SuperString::operator+ (SuperString& st) {
    SuperString res(this->c_str());
    res.append(st.c_str());
    return res;
}

// Overload of the constructor, allows statements such as:
// SuperString st(254);
SuperString::SuperString(int a) {
   this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream() 
   		<< std::dec << a ) ).str());
}

// Converts the SuperString to int, if possible
bool SuperString::toInt(int& i) {
   char c = this->c_str()[0];
   if(empty() || ((!isdigit(c) && (c != '-') && (c != '+')))) return false ;

   char * p ;
   i = strtol(c_str(), &p, 10) ;

   return (*p == 0) ;
}
	

// Evaluates the postfix expression.
// Returns:
//   - result: integer through which we return the result of the eval
//   - boolean return value: true if the expression is valid
bool SuperString::evalPostfix(int &result) {
	//Creates vector and separates them
   	vector<SuperString> V = tokenize(" ");
	//Creates stack for ints 
	stack<int> S;
	int n;
 
	for (int i=0; i < V.size(); i++){
		int a,b; //Used to evaluate the postfix 
		if (V[i].toInt(n))	
			S.push(n);

		else {
			if (S.empty()) return false;
			if (V[i] == "+"){
				//Sets the top to a
				a=S.top();
				S.pop(); //Removes the top element
				
				//If it becomes empty, stops the func
				if (S.empty()) return false;
				//Sets the top to b
				b=S.top(); 
				S.pop();   //Removes the top element
				S.push(b+a); //Pushes the sum of b and a
			}
			if (V[i] == "-"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b-a); //Pushes the sub of b and a
                        }
			if (V[i] == "*"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b*a); //Pushes the mult of b and a
                        }
			if (V[i] == "/"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b/a); //Pushes the div of b and a
                        }

		}
		
	}
	//If the stack contains only one element, returns it
	if (S.size()== 1){
		//Returns the value of the top element in the array
		result = S.top();
		return true;
	}
	//Else, returns false
	else 
		return false;
}

bool SuperString::evalInfix(int &result){
	//Creates vector of tokenized string
	vector<SuperString> V=tokenize(" ");
	//Creates stack to hold operators inside parenthesis
	stack<SuperString> S;
	//Creates stack to hold operators not inside perenthesis
	stack<SuperString> Ss;
	//Used to use toInt
	int n;
	//SuperString used to store the postFix version of the read infix
	SuperString tmp;
	//Determines if a parenthesis has been opened and closed
	bool par=false;
	
	for (int i=0; i < V.size(); i++){
		//If the first elemenet tries to close a parenthesis returns false
		if (i==0 && V[i]==")") return false;
		
		//If the token is an int, concatenates the int to the string
		if(V[i].toInt(n)){
			//If int is on first token, makes the string equal to it
			if (i==0) tmp = V[i];
			//Else, concatenates it
			else tmp = tmp + " " + V[i];

		}
		//Token is not an int
		else {
			//Case it is the opening parenthesis
			if (V[i]=="("){
				S.push(V[i]);
				par = true; //Notifies if it is insie a parenthesis
			}
			//Case it is an operator
			else if (V[i]!="(" && V[i]!=")"){
				//If it is inside a parenthesis:
				if(par){ 
					S.push(V[i]); //Push operand to stack
					
					//If the stak doesn not contain the open parenthesis
					if (S.top()!= "("){
						//If the last token is not an integer and current one is a operand of higher or same priority:
						if (!V[i-1].toInt(n) && (pComp(S.top(),V[i])==SAME_PRTY || pComp(S.top(),V[i])==HIGH_PRTY) ){
							//Concatenate the top of the stack of parenthesis and remove it
							tmp = tmp + " " +S.top();
							S.pop();
							//Push the token to the stack of parenthesis
							S.push(V[i]);
						}
						//If last token isn't int and current one is of lower priority:
						else if (!V[i].toInt(n) && pComp(S.top(),V[i])==LOWER_PRTY){ 
							//Concatenate the current operator
							tmp = tmp + " " + V[i];
						}
					}		
				} 
				//Case it is outside a parenthesis
				else Ss.push(V[i]);
			}

			//Case it is a closing parenthesis
			else if (V[i]==")"){
				par = false; //Notifies it has closed the parenthesis
				//Empties the stack of parenthesis
				while (!S.empty() && S.top()!= "("){
					tmp=tmp + " " +S.top();
					S.pop();
				}
				//Empties the last parenthesis
				S.pop();
			}
		}
	}
	//Stack of parenthesis must be empty, else it will return false
	if (!S.empty())return false;
	//This empties the operators not inside a parenthesis
	while (!Ss.empty() ){
		tmp=tmp + " " + Ss.top();
		Ss.pop();
	}
	//Returns the value of result if the postfix expression is valid
	return tmp.evalPostfix(result);
}
//Function used to compare the priorities of the 
unsigned SuperString::pComp(SuperString a, SuperString b){
	if((a=="/" || a=="*") && (b=="/" || b=="*")) return SAME_PRTY;
	if((a=="/" || a=="*") && (b=="+" || b=="-")) return HIGH_PRTY;	
	if((a=="+" || a=="-") && (b=="+" || b=="-")) return SAME_PRTY;
	if((a=="+" || a=="-") && (b=="/" || b=="*")) return LOWER_PRTY;	
}

