// ====================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es un archivo "header" de la clase SuperString.
//      
// ====================================================================

#ifndef SUPERST_H
#define SUPERST_H

#include <iostream>
#include <stack>
#include <vector>
using namespace std;
const unsigned SAME_PRTY = 0;
const unsigned HIGH_PRTY = 1;
const unsigned LOWER_PRTY = 2;

class SuperString : public string {
public:
	SuperString()  {}
	SuperString(string st) : string(st) {}
	SuperString(int a);
	
	vector<SuperString> tokenize(const string & delim);
	SuperString& operator= (string s);
	SuperString& operator= (int n);
	bool toInt(int& i);
	bool evalPostfix(int &result);
	bool evalInfix(int &result);
	SuperString operator+ (SuperString& st);
	unsigned pComp(SuperString a, SuperString b);
};


#endif

